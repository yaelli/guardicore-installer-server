
var express = require('express');
var bodyParser = require('body-parser');
var _ = require('lodash');
var fs = require('fs');


var app = express();

app.use(express.static('client'));
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var users = [];
var id = -1;

var port = 3000;
app.listen(port, function(){
    console.log('listening on port :', port);
});


var servers = JSON.parse(fs.readFileSync('servers-mock.json', 'utf8'));
var directories = JSON.parse(fs.readFileSync('directory-mock.json', 'utf8'));
console.log(servers.servers[0]);
console.log(directories.data_center_directories.list[1]);

app.get('/servers', function(req, res){
    res.json(servers.servers);
});

app.get('/directories', function(req, res){
    res.json(directories.data_center_directories.list);
});

app.post('/users', function(req, res){
    var user = req.body;
    console.log(user);
    id++;
    user.id = id;
    users.push(user);
    console.log(users);
    res.json(user);
});

